package testcases;

import static org.junit.Assert.*;
import hello.HelloWorld;

import org.junit.Test;

public class SimpleTestCase {

	@Test
	public void test() {
		
		String greeting = "Hello";
		
		HelloWorld helloWorld = new HelloWorld();
		
		helloWorld.setGreeting(greeting);
		
		assertEquals(greeting, helloWorld.getGreeting());
	}

}
